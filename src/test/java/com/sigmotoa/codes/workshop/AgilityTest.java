package com.sigmotoa.codes.workshop;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;

public class AgilityTest {

	@Test
	public void biggerThan() {
		assertTrue(Agility.biggerThan("100", "10"));
		assertTrue(Agility.biggerThan("0.05", "0.005"));
		assertFalse(Agility.biggerThan("10", "100"));
		assertFalse(Agility.biggerThan("0.01", "0.2"));
	}

	@Test
	public void order() {
		assertArrayEquals(new int[] { 2, 4, 8, 10, 15 }, Agility.order(10, 15, 2, 4, 8));
		assertArrayEquals(new int[] { -10, 2, 4, 8, 15 }, Agility.order(-10, 15, 2, 4, 8));
		assertArrayEquals(new int[] { -7, -4, 0, 10, 15 }, Agility.order(0, 15, -7, -4, 10));
	}

	@Test
	public void smallerThan() {
		assertEquals(-1.0, Agility.smallerThan(new double[] { 0.0, 10.1, -1.0, -0.2, 3.2, 200.1 }), 0.0);
		assertEquals(10, Agility.smallerThan(new double[] { 10, 10.1, 20.0, 30.2 }), 0.0);
		assertEquals(-100.1, Agility.smallerThan(new double[] { 3.0, 45.12, 78.0, 0.0, 10.1, -1.0, -0.2, 3.2, -100.1 }),
				0.0);
		assertEquals(-12.0, Agility.smallerThan(new double[] { 0.0, 10.1, -1.0, -0.2, 3.2, 200.1, -12.0 }), 0.0);
	}

	@Test
	public void palindromeNumber() {
		assertTrue(Agility.palindromeNumber(123321));
		assertTrue(Agility.palindromeNumber(1234321));
		assertTrue(Agility.palindromeNumber(1237321));
		assertFalse(Agility.palindromeNumber(123));
		assertFalse(Agility.palindromeNumber(345));
	}

	@Test
	public void palindromeWord() {
		assertTrue(Agility.palindromeWord("civic"));
		assertTrue(Agility.palindromeWord("solos"));
		assertTrue(Agility.palindromeWord("rotator"));
		assertFalse(Agility.palindromeWord("sigmotoa"));
	}

	@Test
	public void factorial() {
		assertEquals(1, Agility.factorial(0));
		assertEquals(1, Agility.factorial(1));
		assertEquals(6, Agility.factorial(3));
		assertEquals(3628800, Agility.factorial(10));
	}

	@Test
	public void isOdd() {
		assertTrue(Agility.isOdd((byte) -7));
		assertTrue(Agility.isOdd((byte) 125));
		assertTrue(Agility.isOdd((byte) 7));
		assertFalse(Agility.isOdd((byte) -2));
		assertFalse(Agility.isOdd((byte) 80));
	}

	@Test
	public void isPrimeNumber() {
		assertFalse(Agility.isPrimeNumber(6));
		assertFalse(Agility.isPrimeNumber(10));
		assertFalse(Agility.isPrimeNumber(-1));
		assertTrue(Agility.isPrimeNumber(2));
		assertTrue(Agility.isPrimeNumber(5));
		assertTrue(Agility.isPrimeNumber(97));
	}

	@Test
	public void isEven() {
		assertTrue(Agility.isEven((byte) -8));
		assertTrue(Agility.isEven((byte) 124));
		assertTrue(Agility.isEven((byte) 14));
		assertFalse(Agility.isEven((byte) -7));
		assertFalse(Agility.isEven((byte) 81));
	}

	@Test
	public void isPerfectNumber() {
		assertTrue(Agility.isPerfectNumber(6));
		assertTrue(Agility.isPerfectNumber(496));
		assertFalse(Agility.isPerfectNumber(100));
		assertFalse(Agility.isPerfectNumber(57));

	}

	@Test
	public void fibonacci() {
		assertArrayEquals(new int[] { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 }, Agility.fibonacci(10));
		assertArrayEquals(new int[] { 0, 1, 1, 2, 3, 5, 8, 13 }, Agility.fibonacci(8));
		assertArrayEquals(new int[] { 0, 1, 1 }, Agility.fibonacci(3));
		assertArrayEquals(new int[] { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 }, Agility.fibonacci(12));
	}

	@Test
	public void timesDividedByThree() {
		assertEquals(1, Agility.timesDividedByThree(3));
		assertEquals(1, Agility.timesDividedByThree(4));
		assertEquals(1, Agility.timesDividedByThree(5));
		assertEquals(2, Agility.timesDividedByThree(6));
		assertEquals(2, Agility.timesDividedByThree(7));

	}

	@Test
	public void fizzBuzz() {
		assertThat(Agility.fizzBuzz(3), is("Fizz"));
		assertThat(Agility.fizzBuzz(6), is("Fizz"));
		assertThat(Agility.fizzBuzz(5), is("Buzz"));
		assertThat(Agility.fizzBuzz(10), is("Buzz"));
		assertThat(Agility.fizzBuzz(15), is("FizzBuzz"));
		assertThat(Agility.fizzBuzz(30), is("FizzBuzz"));
		assertThat(Agility.fizzBuzz(2), is("2"));
		assertThat(Agility.fizzBuzz(16), is("16"));
		assertThat(Agility.fizzBuzz(4), is("4"));

	}
}